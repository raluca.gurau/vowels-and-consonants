#include<iostream>
#include<fstream>
#include<string.h>
using namespace std;


int main()
{

	int n;
	char e;
	char a[50];

	// number of vowels
	int v = 0;
	
	// number of consonants
	int c = 0;

	cin >> n;

	for (int i = 0; i < n; i++)
	{
		cin >> e;
		a[i] = e;
	}
	
	// first option
	for (int i = 0; i < n; i++)
	{

		if (a[i] == 'a')
			v++;
		else if (a[i] == 'e')
			v++;
		else if (a[i] == 'i')
			v++;
		else if (a[i] == 'o')
			v++;
		else if (a[i] == 'u')
			v++;
		else
			c++;
	}

	// second option
	/* 
	while (a[i] != 0)
	{
		switch (a[i])
		{
	        case'a':
			case'e':
			case'i':
			case'o':
			case'u':
			{
				v++;
				break;
			}
			default: 
				c++;
		}
		i++;

	}
	*/

	cout << v << ' ' << c;
	return 0;
}